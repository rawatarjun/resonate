import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'home.dart';

class Signup extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    double _width = MediaQuery.of(context).size.width;
    double _height =MediaQuery.of(context).size.height;
    final String resonate = 'assets/resonate-logo.svg';
    signupButtonOnClick()
    {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => Homepage()),
      );
    }
    onTermsClick()
    {

    }





    // TODO: implement build
    return Scaffold(

      body: SafeArea(
        child: ListView(
          children: <Widget>[
            Row(
                children : <Widget>[
                  Container(
                    height: 10,
                    width: _width ,
                    color: Color.fromRGBO(185, 231, 230,1),
                  ),
                ]
            )
            ,
            Row(
              children: <Widget>[
                Container(
                  height: _height*.06,
                  width: _width,

                )
              ],
            )
            ,
            new SvgPicture.asset(resonate,
              height: _height*.12,
              width: _height*.12,

            ),


            Container(
                height: 5,
                width: _width
            ),
            Column(
              children: <Widget>[
                Container(
                    child:
                    Center(
                        child:Text('Welcome to Resonate',
                          style: TextStyle(fontWeight: FontWeight.bold,
                              fontSize: 35,color: Color.fromRGBO(0, 0, 0, 1)),
                          textAlign: TextAlign.center,

                        )
                    )
                ),

                Container(
                  height: _height*.02,
                ),
                Container(
                    child:
                    Center(
                        child:Text('Where words matter',
                          style: TextStyle(fontWeight: FontWeight.normal,
                              fontSize: 20,color: Color.fromRGBO(68, 68, 68,1)),
                          textAlign: TextAlign.center,

                        )
                    )
                ),
                Container(
                  height: _height*.09,
                ),
              ],
            ),

            //Text Fields Area
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Container(
                    height :_height*.07,
                    width: _width*.45,
                    color: Color.fromRGBO(236,236,236,1),
                    child: TextFormField(

                        style: TextStyle(
                            fontWeight: FontWeight.normal,
                            fontSize: 18,color: Color.fromRGBO(122,122,122,1)
                        ),

                        decoration: InputDecoration(
                          border: OutlineInputBorder(),
                          hintText: ' First Name*',

                        )
                    )
                ),

                Container(
                    height :_height*.07,
                    width: _width*.45,
                    color: Color.fromRGBO(236,236,236,1),
                    child: TextFormField(

                        style: TextStyle(
                            fontWeight: FontWeight.normal,
                            fontSize: 18,color: Color.fromRGBO(122,122,122,1)
                        ),

                        decoration: InputDecoration(
                          border: OutlineInputBorder(),
                          hintText: 'Last Name*',

                        )
                    )
                ),
              ],
            ),
            Container(
              height: _height*.02,
              width: _width,

            ),
            //phone and yob
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[


                Container(
                    height :_height*.07,
                    width: _width*.45,
                    color: Color.fromRGBO(236,236,236,1),
                    child: TextFormField(
                        keyboardType: TextInputType.number,
                        style: TextStyle(
                            fontWeight: FontWeight.normal,
                            fontSize: 18,color: Color.fromRGBO(122,122,122,1)
                        ),

                        decoration: InputDecoration(
                          border: OutlineInputBorder(),
                          hintText: 'Phone',

                        )
                    )
                ),
                Container(
                    height :_height*.07,
                    width: _width*.45,

                    color: Color.fromRGBO(236,236,236,1),
                    child: TextFormField(
                        keyboardType: TextInputType.number,

                        style: TextStyle(
                            fontWeight: FontWeight.normal,
                            fontSize: 18,color: Color.fromRGBO(122,122,122,1)
                        ),

                        decoration: InputDecoration(
                          border: OutlineInputBorder(),
                          hintText: 'YOB',

                        )
                    )
                ),
              ],
            ),
            Container(
              height: _height*.02,
              width: _width,

            ),
            //zipcode
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[


                Container(
                    height :_height*.07,
                    width: _width*.45,
                    color: Color.fromRGBO(236,236,236,1),
                    child: TextFormField(
                        keyboardType: TextInputType.number,
                        style: TextStyle(
                            fontWeight: FontWeight.normal,
                            fontSize: 18,color: Color.fromRGBO(122,122,122,1)
                        ),

                        decoration: InputDecoration(
                          border: OutlineInputBorder(),
                          hintText: 'Zip Code',

                        )
                    )
                ),
                Container(
                    height :_height*.07,
                    width: _width*.45,

                    color: Colors.transparent,
                    child:null
                    )
                ,
              ],
            ),
            Container(
                height :_height*.04,

            ),
            Container(
              width: _width*.7,
              height :_height*.09,
              child: Center(
                  child:  RaisedButton(
                    disabledColor: Colors.grey,


                    onPressed: () {
                      signupButtonOnClick();
                    },

                    textColor: Colors.white,
                    padding: const EdgeInsets.all(0.0),

                    child: Container(
                        width: _width*.7,
                        decoration: const BoxDecoration(

                            color: Color.fromRGBO(185, 231, 230,1)
                        ),
                        padding: const EdgeInsets.all(0.0),


                        child: const Center(


                          child: const Text(
                              'Continue',

                              style: TextStyle(fontSize: 25)
                          ),
                        )

                    ),
                  )
              ),



            ),
            Container(
              height: _height*.04,
            ),
            Container(
                child:
                Center(
                    child:Text("By clicking you agree to Resonate's",
                      style: TextStyle(fontWeight: FontWeight.normal,
                          fontSize: 16,color: Color.fromRGBO(0, 0, 0, .8)),
                      textAlign: TextAlign.center,

                    )
                )
            ),
            Container(
              height: _height*.012,
            ),
            Container(
                child:
                Center(
                    child:GestureDetector(
                      onTap: onTermsClick(),
                      child: Text('Terms & Conditions',

                        style: TextStyle(fontWeight: FontWeight.bold,
                            decoration: TextDecoration.underline,
                            fontSize: 15,color: Color.fromRGBO(0, 0, 0, 1)),
                        textAlign: TextAlign.center,

                      ),
                    )

                )
            ),



          ],
        ),

      ),
    );
  }

}