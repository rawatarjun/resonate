import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_svg/flutter_svg.dart';


class Homepage extends StatefulWidget{
  _MyAppState createState() => _MyAppState();

}

class _MyAppState extends State<Homepage> {

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();


  _showBottomSheetCallback() {
    _scaffoldKey.currentState.showBottomSheet<void>((BuildContext context) {
      double _width = MediaQuery
          .of(context)
          .size
          .width;
      double _height = MediaQuery
          .of(context)
          .size
          .height;
      final String copy = "assets/copy.svg";
      final String settings = "assets/set.svg";
      final String down = "assets/close.svg";
      final String replace= "assets/replace.svg";
      final String refine = "assets/pump.svg";
      final String similar = "assets/equal.svg";
      final String exit ="assets/exit.svg";
      final String camera = "assets/camra.svg";
      onCopyClick()
      {

      }
      onSettingClick()
      {

      }
      onDownClick()
      {
        Navigator.pop(context);
      }
      onReplaceClick()
      {

      }
      onRefineClick()
      {

      }
      onSimilarClick()
      {

      }
      onExitClick()
      {

      }
      onCameraClick()
      {

      }

      return Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
      Container(

      width: _width*.95,
          height: _height * .35,


          decoration:  BoxDecoration(
            color: Colors.transparent,
            borderRadius: BorderRadius.only( bottomLeft :Radius.circular(15),bottomRight: Radius.circular(15),topLeft: Radius.circular(15),topRight: Radius.circular(15) ),


          ),



          child: Center(
              child: Container(
                width: _width*.95,
                height: _height * .35,
                decoration:  BoxDecoration(
                  borderRadius: BorderRadius.only( bottomLeft :Radius.circular(15),bottomRight: Radius.circular(15),topLeft: Radius.circular(15),topRight: Radius.circular(15) ),
                  border: Border(top: BorderSide(color: Colors.black) ,left: BorderSide(color: Colors.black),right: BorderSide(color: Colors.black),bottom:BorderSide(color: Colors.black) ),
                    color: Colors.transparent,

                ),



                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Container(
                        height: _height*.08,
                        width: _width*.90 ,
                        child:Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          mainAxisSize: MainAxisSize.min,

                      children: <Widget>[
                        Container(
//image container
                          height: _height*.08,
                          width: _height*.08,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.only( bottomLeft :Radius.circular(30),bottomRight: Radius.circular(30),topLeft: Radius.circular(30),topRight: Radius.circular(30) ),
                            border: Border(top: BorderSide(color: Colors.black) ,left: BorderSide(color: Colors.black),right: BorderSide(color: Colors.black),bottom:BorderSide(color: Colors.black) ),
                            color: Colors.red,

                          ),

                        ),
                        Container(
                          height: _height*.08,
                          width: _width*.70,

                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            mainAxisSize: MainAxisSize.min,

                            children: <Widget>[

                              Container(
                                height: _height*.03,
                                width: _width*.70,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.only( bottomLeft :Radius.circular(8),bottomRight: Radius.circular(8),topLeft: Radius.circular(8),topRight: Radius.circular(8) ),
                                  border: Border(top: BorderSide(color: Colors.black) ,left: BorderSide(color: Colors.black),right: BorderSide(color: Colors.black),bottom:BorderSide(color: Colors.black) ),
                                  color: Colors.lightBlueAccent,

                                ),
                              ),
                              Container(
                                height: _height*.03,
                                width: _width*.45,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.only( bottomLeft :Radius.circular(8),bottomRight: Radius.circular(8),topLeft: Radius.circular(8),topRight: Radius.circular(8) ),
                                  border: Border(top: BorderSide(color: Colors.black) ,left: BorderSide(color: Colors.black),right: BorderSide(color: Colors.black),bottom:BorderSide(color: Colors.black) ),
                                  color: Colors.lightBlueAccent,

                                ),
                              )
                            ],
                          ),


                        )
                      ],
                    )),
                    Container(height: _height*.08, width: _width*.90 , child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[

                    ClipRect(
                          child: Container(
                            width: _height*.06,
                            height: _height*.06,
                            child: FlatButton(
                              disabledColor: Colors.grey,

                              padding: EdgeInsets.all(0.0),
                              child:SvgPicture.asset(similar,

                                width: _height*.05,
                                height: _height*.05, ),
                              onPressed: () { onSimilarClick();},),

                          ),
                        ),
                    ClipRect(
                          child: Container(
                            width: _height*.06,
                            height: _height*.06,
                            child: FlatButton(
                              disabledColor: Colors.grey,

                              padding: EdgeInsets.all(0.0),
                              child:SvgPicture.asset(camera,

                                width: _height*.05,
                                height: _height*.05, ),
                              onPressed: () { onCameraClick();},),

                          ),
                        ),
                        ClipRect(
                          child: Container(
                            width: _height*.06,
                            height: _height*.06,
                            child: FlatButton(
                              disabledColor: Colors.grey,

                              padding: EdgeInsets.all(0.0),
                              child:SvgPicture.asset(refine,

                                width: _height*.05,
                                height: _height*.05, ),
                              onPressed: () { onRefineClick();},),

                          ),
                        ),
                        ClipRect(
                          child: Container(
                            width: _height*.06,
                            height: _height*.06,
                            child: FlatButton(
                              disabledColor: Colors.grey,

                              padding: EdgeInsets.all(0.0),
                              child:SvgPicture.asset(replace,

                                width: _height*.05,
                                height: _height*.05, ),
                              onPressed: () { onReplaceClick();},),

                          ),
                        ),






                      ],
                    ),),
                    Container(height: _height*.08,width: _width*.90 ,child: Row(
                      children: <Widget>[
                        Text('B')
                      ],
                    ),),
                    Container(
                      height: _height*.08,
                      width: _width*.90 ,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          ClipOval(
                            child: Container(
                              width: _height*.06,
                              height: _height*.06,
                              child: FlatButton(
                                disabledColor: Colors.grey,
                                padding: EdgeInsets.all(0.0),
                                child:SvgPicture.asset(copy,
                                  width: _height*.05,
                                  height: _height*.05,),
                                onPressed: () { onCopyClick();},),



                            ),
                          ),
                          ClipOval(
                            child: Container(
                              width: _height*.06,
                              height: _height*.06,
                              child: FlatButton(
                                disabledColor: Colors.grey,

                                padding: EdgeInsets.all(0.0),
                                child:SvgPicture.asset(settings,
                                  width: _height*.05,
                                  height: _height*.05, ),
                                onPressed: () { onSettingClick();},),

                            ),
                          ),
                          ClipOval(
                            child: Container(
                              width: _height*.06,
                              height: _height*.06,
                              child: FlatButton(
                                disabledColor: Colors.grey,

                                padding: EdgeInsets.all(0.0),
                                child:SvgPicture.asset(down,

                                  width: _height*.05,
                                  height: _height*.05, ),
                                onPressed: () { onDownClick();},),

                            ),
                          ),
                        ],


                      ),
                      decoration:  BoxDecoration(

                          border: Border(top: BorderSide(color: Colors.blueGrey) )

                      ),
                    )


                  ],

                ),
              )
          )

      )
        ],
      );

    });
  }
















  int Index = 0;
  List<Color> _colorhome =[
    Colors.transparent,
    Colors.lightBlueAccent
  ];
  void _homecolor()
  {setState(() {
    if(Index==0)
    {
      Index= 1;
    }
    else
    {
      Index=0;
    }
  });

  }

  @override
  Widget build(BuildContext context) {
    double _width = MediaQuery.of(context).size.width;
    double _height =MediaQuery.of(context).size.height;
    final String up = "assets/up.svg";
    final String home = "assets/home.svg";

    final String user = "assets/user.svg";




    return MaterialApp(
      home:Scaffold(
          key: _scaffoldKey,
          body: Column(
            children: <Widget>[
              Container(
                height: _height*.92 ,
                width: _width,
                child: TextFormField(


                    style: TextStyle(
                        fontWeight: FontWeight.normal,
                        fontSize: 18,
                    ),

                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      hintText: 'Phone',

                    )
                )

              ),
              Container(
                height: _height*.08,
                width: _width,
                  child:Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      ClipRect(

                        child: GestureDetector(
                          onTap: _homecolor,

                          child:Container(
                            width: _height*.08,
                            height: _height*.08,
                           color: _colorhome[Index],

                            child: FlatButton(

                                padding: EdgeInsets.all(0.0),
                                child:SvgPicture.asset(home,
                                  width: _height*.06,
                                  height: _height*.06,),
                              ),



                          ),
                        )
                      ),

                      ClipRect(
                        child: Container(
                          width: _height*.08,
                          height: _height*.08,
                          child: FlatButton(

                            padding: EdgeInsets.all(0.0),
                            child:SvgPicture.asset(up,
                              width: _height*.06,
                              height: _height*.06, ),
                            onPressed:_showBottomSheetCallback ),

                        ),
                      ),

                      ClipOval(
                        child: Container(
                          width: _height*.08,
                          height: _height*.08,
                          child: FlatButton(


                              padding: EdgeInsets.all(0.0),
                              child:SvgPicture.asset(user,
                                width: _height*.06,
                                height: _height*.06, ),
                              onPressed:_showBottomSheetCallback),

                        ),
                      ),




                    ],
                  )
              )

            ],


          )
      ),
    );
  }
}

