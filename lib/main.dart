import 'package:flutter/material.dart';

import 'login.dart';

void main()
{
  runApp(new MaterialApp(
    home: new MyApp()));

}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.

  @override
  Widget build(BuildContext context) {

       return  Scaffold(
         appBar: AppBar(
           title: Text('First Route'),
         ),
         body: Center(
           child: RaisedButton(
             child: Text('Open route'),
             onPressed: () {
               Navigator.push(
                 context,
                 MaterialPageRoute(builder: (context) => Login()),
               );
             },
           ),
         ),
       );

  }
}




