import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:resonate/signup.dart';

class Login extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    double _width = MediaQuery.of(context).size.width;
    double _height =MediaQuery.of(context).size.height;
    final String google = 'assets/google.svg';
    final String twitter = 'assets/twitter.svg';
    final String resonate = 'assets/resonate-logo.svg';
    final String facebook = 'assets/facebook.svg';
    facebookButtonClick()
    {

    }
   googleButtonOnClick()
    {

    }
    twitterButtonOnClick()
    {

    }
    //Login Button Click Operation Click Here
     loginButtonOnClick()
    {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => Signup()),
      );

    }
    onTermsClick()
    {

    }
    return  Scaffold(
        backgroundColor: Color.fromRGBO(255, 255, 255,1),

        body: SafeArea(


            child: ListView(


              children: <Widget>[
                Row(
                  children : <Widget>[
                    Container(
                      height: 10,
                      width: _width ,
                      color: Color.fromRGBO(185, 231, 230,1),
                    ),
                  ]
                )
               ,
                Row(
                  children: <Widget>[
                    Container(
                        height: _height*.06,
                        width: _width,

                    )
                  ],
                )
                ,
                new SvgPicture.asset(resonate,
                      height: _height*.12,
                      width: _height*.12,

                 ),


                 Container(
                    height: 5,
                    width: _width
                ),


                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Container(
                        child:
                        Center(
                            child:Text('Welcome to Resonate',
                              style: TextStyle(fontWeight: FontWeight.bold,
                                  fontSize: 35,color: Color.fromRGBO(0, 0, 0, 1)),
                              textAlign: TextAlign.center,

                            )
                        )
                    ),

                    Container(
                      height: _height*.02,
                    ),
                    Container(
                        child:
                        Center(
                            child:Text('Where words matter',
                              style: TextStyle(fontWeight: FontWeight.normal,
                                  fontSize: 20,color: Color.fromRGBO(68, 68, 68,1)),
                              textAlign: TextAlign.center,

                            )
                        )
                    ),
                    Container(
                      height: _height*.02,
                    ),

                    Container(
                        child:
                        Center(
                            child:Text('Sign in using',
                              style: TextStyle(fontWeight: FontWeight.normal,
                                  fontSize: 18,color: Color.fromRGBO(114, 114, 114,1)),
                              textAlign: TextAlign.center,

                            )
                        )
                    ),

                    Container(

                      child: Center(
                          child:Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              ClipOval(
                                child: Container(
                                  width: _height*.08,
                                  height: _height*.08,
                                  child: FlatButton(
                                    disabledColor: Colors.grey,
                                    padding: EdgeInsets.all(0.0),
                                    child:SvgPicture.asset(google,
                                      width: _height*.06,
                                      height: _height*.06,),
                                    onPressed: () { googleButtonOnClick();},),



                                ),
                              ),
                              ClipOval(
                                child: Container(
                                  width: _height*.08,
                                  height: _height*.08,
                                  child: FlatButton(
                                    disabledColor: Colors.grey,

                                    padding: EdgeInsets.all(0.0),
                                    child:SvgPicture.asset(facebook,
                                      width: _height*.06,
                                      height: _height*.06, ),
                                    onPressed: () { facebookButtonClick();},),

                                ),
                              ),
                              ClipOval(
                                child: Container(
                                  width: _height*.08,
                                  height: _height*.08,
                                  child: FlatButton(
                                    disabledColor: Colors.grey,

                                    padding: EdgeInsets.all(0.0),
                                         child:SvgPicture.asset(twitter,
                                           width: _height*.06,
                                           height: _height*.06, ),
                                      onPressed: () { twitterButtonOnClick();},),
                                  
                                ),
                              ),




                            ],
                          )
                      ),
                    ),

                    Container(
                        child:
                        Center(
                            child:Text('Or',
                              style: TextStyle(fontWeight: FontWeight.normal,
                                  fontSize: 18,color: Color.fromRGBO(114, 114, 114,1)),
                              textAlign: TextAlign.center,

                            )
                        )
                    ),
                    Container(
                      height: _height*.015,
                    ),
                    Container(
                        height :_height*.09,
                        width: _width*.7,
                        color: Color.fromRGBO(236,236,236,1),
                        child: TextFormField(
                            keyboardType: TextInputType.emailAddress,

                            style: TextStyle(

                                fontWeight: FontWeight.normal,
                                fontSize: 22,color: Color.fromRGBO(122,122,122,1)
                            ),

                            decoration: InputDecoration(
                            border: OutlineInputBorder(),
                      hintText: ' Email',

                    )
                        )
                    ),
                    Container(
                      height: _height*.015,
                    ),
                    Container(
                        height :_height*.09,
                        width: _width*.7,
                        color: Color.fromRGBO(236,236,236,1),
                        child: TextFormField(
                            keyboardType: TextInputType.visiblePassword,


                            style: TextStyle(
                                fontWeight: FontWeight.normal,
                                fontSize: 22,color: Color.fromRGBO(122,122,122,1)
                            ),

                            decoration: InputDecoration(
                              border: OutlineInputBorder(),
                              hintText: 'Password',

                            )
                        )
                    ),
                    Container(
                      height: _height*.015,
                    ),
                    Container(
                      width: _width*.7,
                        height :_height*.09,

                        child:  RaisedButton(
                          disabledColor: Colors.grey,

                          onPressed: () {
                                    loginButtonOnClick();
                          },

                          textColor: Colors.white,
                          padding: const EdgeInsets.all(0.0),

                          child: Container(
                            width: _width*.7,
                            decoration: const BoxDecoration(

                             color: Color.fromRGBO(185, 231, 230,1)
                            ),
                            padding: const EdgeInsets.all(0.0),


                            child: const Center(

                                    child: const Text(

                                        'Continue',
                                        style: TextStyle(fontSize: 25)
                                    ),
    )

                          ),
                        ),
                    ),
                    Container(
                      height: _height*.04,
                    ),
                    Container(
                        child:
                        Center(
                            child:Text("By clicking you agree to Resonate's",
                              style: TextStyle(fontWeight: FontWeight.normal,
                                  fontSize: 16,color: Color.fromRGBO(0, 0, 0, .8)),
                              textAlign: TextAlign.center,

                            )
                        )
                    ),
                    Container(
                      height: _height*.012,
                    ),
                    Container(
                        child:
                        Center(
                            child:GestureDetector(
                              onTap: onTermsClick(),
                        child: Text('Terms & Conditions',

                          style: TextStyle(fontWeight: FontWeight.bold,
                              decoration: TextDecoration.underline,
                              fontSize: 15,color: Color.fromRGBO(0, 0, 0, 1)),
                          textAlign: TextAlign.center,

                        ),
                        )

                        )
                    ),

                  ],
                )

              ],


            ),
          ),
        );


  }

}